#include "runtime_options.h"

#include <stdlib.h>
#include <unistd.h>

void read_options(int a_argc,
			char **a_argv,
			const char *a_options,
			int a_options_count,
			int *a_flags)
{
	int opt;
	while(-1 != (opt = getopt(a_argc, a_argv, a_options)))
	{
		unsigned flag_iterator = 0;
		while(a_options[flag_iterator] != opt && a_options_count > flag_iterator)
		{
			++flag_iterator;
		}
		if(a_options_count > flag_iterator)
		{
			a_flags[flag_iterator] = optarg ? atoi(optarg) : 1;
		}
	}
}
