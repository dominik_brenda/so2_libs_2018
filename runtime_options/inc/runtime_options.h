#ifndef RUNTIME_OPTIONS_H
#define RUNTIME_OPTIONS_H

/**
 *	@brief void read_options
		Reads runtime options and sets corresponding flags accordingly
	@param a_argc		int	counter of arguments
	@param a_argv		char**	array of arguments
	@param a_options	char*	cstring containing all options used by program
	@param a_options_count	int	number of options
	@param a_flags		int*	array of flags to set
 */
void read_options(int a_argc,
			char **a_argv,
			const char* a_options,
			int a_options_count,
			int *a_flags);

#endif
