#include <dlfcn.h>
#include <stdio.h>

#include "dynlibhandle.h"

void link_lib(const char *a_lib_name,
				void **a_lib_handle,
				unsigned *a_flags,
				const int a_flag_index,
				const int a_flags_size)
{
	*a_lib_handle = dlopen(a_lib_name, RTLD_LAZY | RTLD_LOCAL);
	if(!*a_lib_handle)
	{
		printf("Could not load library: %s\n", a_lib_name);
		if(a_flags && a_flag_index < a_flags_size)
		{
			a_flags[a_flag_index] = 0;
		}
	}
}

void link_func(void *a_lib_handle,
				const char *a_func_name,
				char* (**a_func)(char*),
				unsigned *a_flags,
				const int a_flag_index,
				const int a_flags_size)
{
	*a_func = (char*(*)(char*))(dlsym(a_lib_handle, a_func_name));
	if(!*a_func)
	{
		printf("Could not load function: %s\n", a_func_name);
		if(a_flags && a_flag_index < a_flags_size)
		{
			a_flags[a_flag_index] = 0;
		}
	}
}

void link_lib_close(void **a_lib_handle)
{
	if(a_lib_handle)
	{
		dlclose(a_lib_handle);
	}
}
