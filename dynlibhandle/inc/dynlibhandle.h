#ifndef DYNLIBHANDLE_H
#define DYNLIBHANDLE_H
/*
 *  @brief link_lib	void
 *  @desc  links shared library and sets corresponding flag to 0 if the linking was unsuccesful
 *  @param a_lib_name	char*		name of the library to be linked
 *  @param a_lib_handle void**		handle for shared library
 *  @param a_flags		unsigned*	array of flags, if no flag is to be changed set a_flags to NULL
 *  @param a_flag_index	int			index of flag to be changed if the linking fails
 *  @param a_flags_size int			size of a_flags array
 */
void link_lib(const char *a_lib_name,
				void **a_lib_handle,
				unsigned *a_flags,
				const int a_flag_index,
				const int a_flag_size);

/*
 *  @brief link_func	void
 *  @desc  links function from shared library and sets corresponding flag to 0 if the linking was unsuccesful
 *  @param a_lib_handle void**				handle for shared library
 *  @param a_func_name	char*				name of the function to be linked
 *  @param a_func		void*(char*, char*)	function pointer pointing at linked function if the linking is succesful
 *  @param a_flags		unsigned*			array of flags, if no flag is to be changed set a_flags to NULL
 *  @param a_flag_index	int					index of flag to be changed if the linking fails
 *  @param a_flags_size int					size of a_flags array
 */
void link_func(void *a_lib_handle,
				const char *a_func_name,
				char* (**a_func)(char*),
				unsigned *a_flags,
				const int a_flag_index,
				const int a_flag_size);

/*
 *  @brief	link_lib_close	void
 *  @desc closes link to shared library
 *  @param	a_lib_handle	void**	handle to library to be closed
 */
void link_lib_close(void **a_lib_handle);

#endif
