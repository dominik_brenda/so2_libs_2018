#include "grouplist.h"

#include<grp.h>
#include<pwd.h>
#include<stdlib.h>
#include<string.h>

#ifndef STUB_LIB
char *get_user_groups(char *a_user_name)
{
	// number of groups of the user; set to correct number by first getgrouplist invocation
	int number_of_groups = 0;
	// passwd struct of a user, needed to get user's main group
	struct passwd *pwd = getpwnam(a_user_name);
	if(NULL == pwd)return NULL;
	gid_t *groups = NULL;
	// first invocation sets number_of_groups to correct value
	getgrouplist(a_user_name, pwd->pw_gid, groups, &number_of_groups);
	groups = malloc(sizeof(gid_t) * number_of_groups);
	// second invocation fills gid_t array with user's groups data
	getgrouplist(a_user_name, pwd->pw_gid, groups, &number_of_groups);
	// length of a result cstring;
	// 2 square braces and terminal null plus comma and space for each group above one
	int result_len = 3 + 2 * (number_of_groups - 1);
	for(int group_counter = 0; group_counter < number_of_groups; ++group_counter)
	{
		result_len += strlen(getgrgid(groups[group_counter])->gr_name);
	}
	char *result = malloc(sizeof(char) * result_len);
	strcpy(result, "[");
	for(int group_counter = 0; group_counter < number_of_groups; ++group_counter)
	{
		strcat(result, getgrgid(groups[group_counter])->gr_name);
		if(group_counter < number_of_groups - 1)
		{
			strcat(result, ", ");
		}
	}
	free(groups);
	strcat(result, "]");
	return result;
}
#endif
