#ifndef GROUPLIST_H
#define GROUPLIST_H

#ifndef STUB_LIB
/**
 *  @brief get_user_groups	char*
 *  @desc gets all groups of a user defined by username and compiles them in a formated cstring;
 *  returned cstring needs to be freed
 *  @param a_user_name	char*	username of a users whose groups are to be printed
 *  @return char*	formated cstring storing all user's groups
 */
char* get_user_groups(char *a_user_name);
#endif

#endif
